<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU" sourcelanguage="en_US">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../aboutdlg.ui" line="20"/>
        <source>About crqt-ng</source>
        <translation>О программе crqt-ng</translation>
    </message>
    <message>
        <location filename="../aboutdlg.ui" line="79"/>
        <source>License</source>
        <translation>Лицензия</translation>
    </message>
    <message>
        <location filename="../aboutdlg.ui" line="58"/>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="44"/>
        <source>crqt-ng is free open source e-book viewer based on crengine-ng library.</source>
        <translation>crqt-ng - это свободная программа с открытым исходным кодом для просмотра электронных книг основанная на движке crengine-ng.</translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="45"/>
        <source>Source code is available at</source>
        <translation>Исходный код доступен на</translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="46"/>
        <source>under the terms of GNU GPL license either version 2 or (at your option) any later version.</source>
        <translation>под лицензией GNU GPL2 или (на ваш выбор) более поздней.</translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="48"/>
        <source>It is a fork of the &apos;CoolReader&apos; program.</source>
        <translation>Является форком программы &apos;CoolReader&apos;.</translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="50"/>
        <source>Third party components used in crengine-ng:</source>
        <translation>В crengine-ng использованы следующие сторонние компоненты:</translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="53"/>
        <source>FreeType - font rendering</source>
        <translation>FreeType - рендеринг шрифтов</translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="56"/>
        <source>HarfBuzz - text shaping, font kerning, ligatures</source>
        <translation>HarfBuzz - формировщик текста, кернинг, лигатуры</translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="59"/>
        <source>ZLib - compressing library</source>
        <translation>ZLib - библиотека сжатия</translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="62"/>
        <source>ZSTD - compressing library</source>
        <translation>ZSTD - библиотека сжатия</translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="65"/>
        <source>libpng - PNG image format support</source>
        <translation>libpng - библиотека поддержки изображений формата PNG</translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="68"/>
        <source>libjpeg - JPEG image format support</source>
        <translation>libjpeg - библиотека поддержки изображений формата JPEG</translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="71"/>
        <source>FriBiDi - RTL writing support</source>
        <translation>FriBiDi - поддержка RTL письменности</translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="74"/>
        <source>libunibreak - line breaking and word breaking algorithms</source>
        <translation>libunibreak - алгоритмы разбиения строк и слов</translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="77"/>
        <source>utf8proc - for unicode string comparision</source>
        <translation>utf8proc - для сравнения строк юникода</translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="80"/>
        <source>NanoSVG - SVG image format support</source>
        <translation>NanoSVG - библиотека поддержки изображений формата SVG</translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="83"/>
        <source>chmlib - chm format support</source>
        <translation>chmlib - библиотека поддержки формата CHM</translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="86"/>
        <source>antiword - Microsoft Word format support</source>
        <translation>antiword - библиотека поддержки документов Microsoft Word</translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="89"/>
        <source>RFC6234 (sources) - SHAsum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="92"/>
        <source>cmark - CommonMark parsing and rendering library and program in C</source>
        <translation>cmark - CommonMark библиотека парсинга и рендеринга и программа на C</translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="96"/>
        <source>cmark-gfm - GitHub&apos;s fork of cmark, a CommonMark parsing and rendering library and program in C</source>
        <translation>cmark-gfm - форк cmark от GitHub, CommonMark библиотеки парсинга и рендеринга и программы на C</translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="101"/>
        <source>MD4C - MD4C stands for &quot;Markdown for C&quot; and that&apos;s exactly what this project is about.</source>
        <translation>MD4C - MD4C означает &quot;Markdown for C&quot; и это точное описание этого проекта.</translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="104"/>
        <source>hyphman - AlReader hyphenation manager</source>
        <translation>hyphman - система переносов из AlReader</translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="105"/>
        <source>Most hyphenation dictionaries - TEX hyphenation patterns</source>
        <translation>Основные словари переносов - TEX hyphenation patterns</translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="106"/>
        <source>Russian hyphenation dictionary - </source>
        <translation>Русский словарь переносов - </translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="109"/>
        <source>Languages character set database by Fontconfig</source>
        <translation>Набор символов различных письменностей от Fontconfig</translation>
    </message>
</context>
<context>
    <name>AddBookmarkDialog</name>
    <message>
        <location filename="../addbookmarkdlg.cpp" line="49"/>
        <source>Add bookmark</source>
        <translation>Добавить закладку</translation>
    </message>
    <message>
        <location filename="../addbookmarkdlg.cpp" line="57"/>
        <location filename="../addbookmarkdlg.cpp" line="126"/>
        <source>Correction</source>
        <translation>Исправление</translation>
    </message>
    <message>
        <location filename="../addbookmarkdlg.ui" line="22"/>
        <source>Bookmark type</source>
        <translation>Тип закладки</translation>
    </message>
    <message>
        <location filename="../addbookmarkdlg.ui" line="66"/>
        <source>Title</source>
        <translation>Название</translation>
    </message>
    <message>
        <location filename="../addbookmarkdlg.ui" line="32"/>
        <location filename="../addbookmarkdlg.cpp" line="54"/>
        <source>Position</source>
        <translation>Позиция</translation>
    </message>
    <message>
        <location filename="../addbookmarkdlg.ui" line="46"/>
        <source>Position text</source>
        <translation>Позиция текста</translation>
    </message>
    <message>
        <location filename="../addbookmarkdlg.ui" line="53"/>
        <location filename="../addbookmarkdlg.cpp" line="56"/>
        <location filename="../addbookmarkdlg.cpp" line="122"/>
        <source>Comment</source>
        <translation>Комментарии</translation>
    </message>
    <message>
        <location filename="../addbookmarkdlg.ui" line="14"/>
        <source>Dialog</source>
        <translation>Диалог</translation>
    </message>
    <message>
        <location filename="../addbookmarkdlg.ui" line="39"/>
        <source>Page 1/10 10%</source>
        <translation>Страница 1/10 10%</translation>
    </message>
    <message>
        <location filename="../addbookmarkdlg.ui" line="73"/>
        <source>sample title</source>
        <translation>пример заголовка</translation>
    </message>
</context>
<context>
    <name>BookmarkListDialog</name>
    <message>
        <location filename="../bookmarklistdlg.cpp" line="46"/>
        <source>Bookmarks</source>
        <translation>Закладки</translation>
    </message>
    <message>
        <location filename="../bookmarklistdlg.cpp" line="50"/>
        <source>Position</source>
        <translation>Позиция</translation>
    </message>
    <message>
        <location filename="../bookmarklistdlg.cpp" line="50"/>
        <source>Type</source>
        <comment>bookmark type</comment>
        <translation>Тип закладки</translation>
    </message>
    <message>
        <location filename="../bookmarklistdlg.cpp" line="50"/>
        <source>Text</source>
        <translation>Текст</translation>
    </message>
    <message>
        <location filename="../bookmarklistdlg.cpp" line="51"/>
        <source>Comment</source>
        <translation>Комментарии</translation>
    </message>
    <message>
        <location filename="../bookmarklistdlg.cpp" line="88"/>
        <source>P</source>
        <comment>Bookmark type first letter - Position</comment>
        <translation>П</translation>
    </message>
    <message>
        <location filename="../bookmarklistdlg.cpp" line="90"/>
        <source>C</source>
        <comment>Bookmark type first letter - Comment</comment>
        <translation>К</translation>
    </message>
    <message>
        <location filename="../bookmarklistdlg.cpp" line="92"/>
        <source>E</source>
        <comment>Bookmark type first letter - Correction/Edit</comment>
        <translation>И</translation>
    </message>
    <message>
        <location filename="../bookmarklistdlg.ui" line="49"/>
        <source>Go to Bookmark</source>
        <translation>Перейти к закладке</translation>
    </message>
    <message>
        <location filename="../bookmarklistdlg.ui" line="57"/>
        <source>Remove Bookmark</source>
        <translation>Удалить закладку</translation>
    </message>
    <message>
        <location filename="../bookmarklistdlg.ui" line="65"/>
        <source>Remove ALL Bookmarks</source>
        <translation>Удалить все закладки</translation>
    </message>
    <message>
        <location filename="../bookmarklistdlg.ui" line="70"/>
        <source>Edit Bookmark</source>
        <translation>Редактировать закладку</translation>
    </message>
    <message>
        <location filename="../bookmarklistdlg.ui" line="78"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <location filename="../bookmarklistdlg.ui" line="14"/>
        <source>Dialog</source>
        <translation>Диалог</translation>
    </message>
    <message>
        <location filename="../bookmarklistdlg.ui" line="52"/>
        <source>Return</source>
        <translation>Return</translation>
    </message>
    <message>
        <location filename="../bookmarklistdlg.ui" line="60"/>
        <source>Del</source>
        <translation>Del</translation>
    </message>
    <message>
        <location filename="../bookmarklistdlg.ui" line="73"/>
        <source>F4</source>
        <translation>F4</translation>
    </message>
    <message>
        <location filename="../bookmarklistdlg.ui" line="81"/>
        <source>Esc</source>
        <translation>Esc</translation>
    </message>
</context>
<context>
    <name>CR3View</name>
    <message>
        <location filename="../cr3widget.cpp" line="1316"/>
        <source>Error while opening document </source>
        <translation>Ошибка открытия документа </translation>
    </message>
    <message>
        <location filename="../cr3widget.cpp" line="1719"/>
        <source>Loading: please wait...</source>
        <translation>Загрузка: подождите...</translation>
    </message>
    <message>
        <location filename="../cr3widget.cpp" line="1202"/>
        <source>Warning</source>
        <translation>Предупреждение</translation>
    </message>
    <message>
        <location filename="../cr3widget.cpp" line="1203"/>
        <source>Font &quot;%1&quot; isn&apos;t compatible with language &quot;%2&quot;. Instead will be used fallback font.</source>
        <translation>Шрифт &quot;%1&quot; не совместим с языком &quot;%2&quot;. Вместо него будет задействован дополнительный шрифт.</translation>
    </message>
</context>
<context>
    <name>ExportProgressDlg</name>
    <message>
        <location filename="../exportprogressdlg.ui" line="14"/>
        <source>Dialog</source>
        <translation>Диалог</translation>
    </message>
    <message>
        <location filename="../exportprogressdlg.ui" line="20"/>
        <source>Export is in progress...</source>
        <translation>Идет экспорт...</translation>
    </message>
</context>
<context>
    <name>FallbackFontsDialog</name>
    <message>
        <location filename="../fallbackfontsdialog.ui" line="14"/>
        <source>Fallback fonts</source>
        <translation>Дополнительные шрифты</translation>
    </message>
    <message>
        <location filename="../fallbackfontsdialog.ui" line="23"/>
        <source>List of fallback fonts:</source>
        <translation>Список дополнительных шрифтов:</translation>
    </message>
    <message>
        <location filename="../fallbackfontsdialog.cpp" line="163"/>
        <source>Remove this fallback font</source>
        <translation>Удалить этот дополнительный шрифт</translation>
    </message>
</context>
<context>
    <name>FilePropsDialog</name>
    <message>
        <location filename="../filepropsdlg.cpp" line="41"/>
        <source>Document properties</source>
        <translation>Свойства документа</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="44"/>
        <source>Property</source>
        <translation>Свойство</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="44"/>
        <source>Value</source>
        <translation>Значение</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="186"/>
        <source>Current page</source>
        <translation>Текущая страница</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="187"/>
        <source>Total pages</source>
        <translation>Всего страниц</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="188"/>
        <source>Battery state</source>
        <translation>Состояние батареи</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="189"/>
        <source>Current Time</source>
        <translation>Текущее время</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="194"/>
        <source>Current chapter</source>
        <translation>Глава</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="197"/>
        <source>Status</source>
        <translation>Статус</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="199"/>
        <source>Archive name</source>
        <translation>Имя файла архива</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="200"/>
        <source>Archive path</source>
        <translation>Путь к архиву</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="201"/>
        <source>Archive size</source>
        <translation>Размер архива</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="202"/>
        <source>File name</source>
        <translation>Имя файла</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="203"/>
        <source>File path</source>
        <translation>Путь к файлу</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="204"/>
        <source>File size</source>
        <translation>Размер файла</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="205"/>
        <source>File format</source>
        <translation>Формат файла</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="206"/>
        <source>File info</source>
        <translation>Информация о файле</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="208"/>
        <source>Title</source>
        <translation>Название</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="209"/>
        <source>Author(s)</source>
        <translation>Автор(ы)</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="210"/>
        <source>Series name</source>
        <translation>Серия</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="211"/>
        <source>Series number</source>
        <translation>Номер в серии</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="212"/>
        <source>Date</source>
        <translation>Дата</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="213"/>
        <source>Genres</source>
        <translation>Жанры</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="215"/>
        <source>Translator</source>
        <translation>Перевод</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="216"/>
        <source>Book info</source>
        <translation>Информация о книге</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="218"/>
        <source>Document author</source>
        <translation>Автор документа</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="219"/>
        <source>Document date</source>
        <translation>Дата документа</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="220"/>
        <source>Document source URL</source>
        <translation>URL исходного документа</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="221"/>
        <source>OCR by</source>
        <translation>OCR</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="222"/>
        <source>Document version</source>
        <translation>Версия документа</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="223"/>
        <source>Document info</source>
        <translation>Информация о документе</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="225"/>
        <source>Publication name</source>
        <translation>Название публикации</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="226"/>
        <source>Publisher</source>
        <translation>Издательство</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="227"/>
        <source>Publisher city</source>
        <translation>Город</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="228"/>
        <source>Publication year</source>
        <translation>Год издания</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="229"/>
        <source>ISBN</source>
        <translation>ISBN</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="230"/>
        <source>Publication info</source>
        <translation>Информация о публикации</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="232"/>
        <source>Custom info</source>
        <translation>Прочая информация</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.ui" line="14"/>
        <source>Dialog</source>
        <translation>Диалог</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="214"/>
        <source>Language</source>
        <translation>Язык</translation>
    </message>
</context>
<context>
    <name>FontFamiliesDialog</name>
    <message>
        <location filename="../fontfamiliesdialog.ui" line="23"/>
        <source>Generic font families</source>
        <translation>Универсальные семейства шрифтов</translation>
    </message>
    <message>
        <location filename="../fontfamiliesdialog.ui" line="36"/>
        <source>Generic font families settings</source>
        <translation>Настройка универсальных семейств шрифтов</translation>
    </message>
    <message>
        <location filename="../fontfamiliesdialog.ui" line="45"/>
        <source>Serif</source>
        <translation>С засечками</translation>
    </message>
    <message>
        <location filename="../fontfamiliesdialog.ui" line="62"/>
        <source>Sans-Serif</source>
        <translation>Без засечек</translation>
    </message>
    <message>
        <location filename="../fontfamiliesdialog.ui" line="79"/>
        <source>Cursive</source>
        <translation>Курсивный</translation>
    </message>
    <message>
        <location filename="../fontfamiliesdialog.ui" line="96"/>
        <source>Fantasy</source>
        <translation>Фэнтезийный</translation>
    </message>
    <message>
        <location filename="../fontfamiliesdialog.ui" line="113"/>
        <source>Monospace</source>
        <translation>Моноширинный</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.cpp" line="291"/>
        <source>Open book file</source>
        <translation>Открыть книгу</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="356"/>
        <source>Export document to</source>
        <translation>Экспортировать документ в</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="189"/>
        <location filename="../mainwindow.cpp" line="1030"/>
        <location filename="../mainwindow.cpp" line="1055"/>
        <source>Warning</source>
        <translation>Предупреждение</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="189"/>
        <location filename="../mainwindow.cpp" line="1030"/>
        <location filename="../mainwindow.cpp" line="1055"/>
        <source>The maximum number of tabs has been exceeded!</source>
        <translation>Превышего максимальное количество вкладок!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="356"/>
        <source>WOL book (*.wol)</source>
        <translation>Книга WOL (.wol)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="361"/>
        <source>Export to WOL format</source>
        <translation>Экспортировать в WOL формат</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="293"/>
        <source>All supported formats</source>
        <translation>Все форматы</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="294"/>
        <source>FB2 books</source>
        <translation>Книги FB2</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="296"/>
        <source>Text files</source>
        <translation>Текстовые файлы</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="297"/>
        <source>Rich text</source>
        <translation>Текст RTF</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="298"/>
        <source>MS Word document</source>
        <translation>Документы MS Word</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="300"/>
        <source>HTML files</source>
        <translation>Файлы HTML</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="302"/>
        <source>Markdown files</source>
        <translation>файлы Markdown</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="304"/>
        <source>EPUB files</source>
        <translation>Книги EPUB</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="306"/>
        <source>CHM files</source>
        <translation>Файлы CHM</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="308"/>
        <source>MOBI files</source>
        <translation>Файлы MOBI</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="309"/>
        <source>PalmDOC files</source>
        <translation>Файлы PDB/PalmDoc</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="310"/>
        <source>ZIP archives</source>
        <translation>Архивы ZIP</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="295"/>
        <source>FB3 books</source>
        <translation>Книги FB3</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="299"/>
        <source>Open Document files</source>
        <translation>Файлы Open Document</translation>
    </message>
</context>
<context>
    <name>MainWindowClass</name>
    <message>
        <location filename="../mainwindow.ui" line="78"/>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="93"/>
        <source>View</source>
        <translation>Вид</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="109"/>
        <source>Navigation</source>
        <translation>Навигация</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="125"/>
        <source>Help</source>
        <translation>Справка</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="169"/>
        <source>Open...</source>
        <translation>Открыть...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="172"/>
        <source>Open file</source>
        <translation>Открыть файл</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="175"/>
        <source>Ctrl+O</source>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="180"/>
        <source>Toggle Pages/Scroll</source>
        <translation>Переключить вид Страница/Свиток</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="183"/>
        <source>Ctrl+T</source>
        <translation>Ctrl+T</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="192"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="195"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="203"/>
        <source>Minimize</source>
        <translation>Свернуть</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="218"/>
        <source>Page Down</source>
        <translation>На страницу вниз</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="233"/>
        <source>Page Up</source>
        <translation>На страницу вверх</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="244"/>
        <source>Line Down</source>
        <translation>На строку вниз</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="255"/>
        <source>Line Up</source>
        <translation>На строку вверх</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="266"/>
        <source>First Page</source>
        <translation>Первая страница</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="269"/>
        <source>Go to first page</source>
        <translation>Перейти на первую страницу</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="277"/>
        <source>Last Page</source>
        <translation>Последняя страница</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="283"/>
        <source>End</source>
        <translation>End</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="292"/>
        <source>Back</source>
        <translation>Назад</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="307"/>
        <source>Forward</source>
        <translation>Вперед</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="318"/>
        <source>Next Chapter</source>
        <translation>Следующая глава</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="329"/>
        <source>Previous Chapter</source>
        <translation>Предыдущая глава</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="347"/>
        <source>Toggle Full Screen</source>
        <translation>Полно-экранный режим</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="350"/>
        <source>Toggle Full Screen mode</source>
        <translation>Полно-экранный режим</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="362"/>
        <source>Zoom In</source>
        <translation>Увеличить</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="365"/>
        <source>Increase font size</source>
        <translation>Увеличить размер шрифта</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="377"/>
        <source>Zoom Out</source>
        <translation>Уменьшить</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="380"/>
        <source>Decrease font size</source>
        <translation>Уменьшить размер шрифта</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="392"/>
        <source>Table of Contents...</source>
        <translation>Содержание...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="395"/>
        <source>Show table of contents</source>
        <translation>Показать содержание</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="407"/>
        <source>Recent Books</source>
        <translation>Последние книги</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="410"/>
        <source>Show recent books list</source>
        <translation>Показать список последних книг</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="422"/>
        <source>Settings...</source>
        <translation>Настройки...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="425"/>
        <source>Settings dialog</source>
        <translation>Диалог настроек</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="437"/>
        <source>Copy</source>
        <translation>Копировать</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="440"/>
        <source>Copy selected text</source>
        <translation>Копировать выделение</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="459"/>
        <source>About Qt...</source>
        <translation>О Qt...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="462"/>
        <source>About Qt</source>
        <translation>О Qt</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="473"/>
        <source>About the program</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="485"/>
        <source>Add Bookmark</source>
        <translation>Добавить закладку</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="488"/>
        <source>Add bookmark</source>
        <translation>Добавить закладку</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="500"/>
        <source>Bookmark List...</source>
        <translation>Список закладок...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="503"/>
        <source>Show bookmarks list</source>
        <translation>Показать список закладок</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="515"/>
        <source>File Properties...</source>
        <translation>Свойства документа...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="518"/>
        <source>Show file properties</source>
        <translation>Свойства документа</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="530"/>
        <source>Rotate</source>
        <translation>Поворот страницы</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="542"/>
        <source>Find text...</source>
        <translation>Найти текст...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="545"/>
        <source>Find text</source>
        <translation>Найти текст</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="631"/>
        <source>New tab</source>
        <translation>Новая вкладка</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="634"/>
        <source>Open new tab</source>
        <translation>Открыть новую вкладку</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="637"/>
        <source>Ctrl+Shift+T</source>
        <translation>Ctrl+Shift+T</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="646"/>
        <source>Open in new tab...</source>
        <translation>Открыть в новой вкладке...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="655"/>
        <location filename="../mainwindow.ui" line="658"/>
        <source>Open link in new tab</source>
        <translation>Открыть ссылку в новой вкладке</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="206"/>
        <source>Esc</source>
        <translation>Esc</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="14"/>
        <source>crqt-ng</source>
        <translation>crqt-ng</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="221"/>
        <source>Go to next page</source>
        <translation>Перейти на следующую страницу</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="224"/>
        <source>PgDown</source>
        <translation>PgDown</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="236"/>
        <source>Back by page</source>
        <translation>Назад на страницу</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="239"/>
        <source>PgUp</source>
        <translation>PgUp</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="247"/>
        <source>Forward by one line</source>
        <translation>Вперед на одну линию</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="250"/>
        <source>Down</source>
        <translation>Down</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="258"/>
        <source>Back by line</source>
        <translation>Назад на линию</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="261"/>
        <source>Up</source>
        <translation>Up</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="272"/>
        <source>Home</source>
        <translation>Home</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="280"/>
        <source>Go to last page</source>
        <translation>Перейти у последней странице</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="295"/>
        <source>Back in navigation history</source>
        <translation>Назад по истории навигации</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="298"/>
        <source>Backspace</source>
        <translation>Backspace</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="310"/>
        <source>Go to the next position in navigation history</source>
        <translation>Перейти к следующей позиции в истории навигации</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="313"/>
        <source>Shift+Backspace</source>
        <translation>Shift+Backspace</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="321"/>
        <source>Go to next chapter</source>
        <translation>Перейти к следующей главе</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="324"/>
        <source>Alt+Down</source>
        <translation>Alt+Down</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="332"/>
        <source>Go to previous chapter</source>
        <translation>Перейти к предыдущей главе</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="335"/>
        <source>Alt+Up</source>
        <translation>Alt+Up</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="353"/>
        <source>Alt+Return</source>
        <translation>Alt+Return</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="368"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="383"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="398"/>
        <source>F4</source>
        <translation>F4</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="413"/>
        <source>F5</source>
        <translation>F5</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="428"/>
        <source>F9</source>
        <translation>F9</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="443"/>
        <source>Ctrl+C</source>
        <translation>Ctrl+C</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="448"/>
        <source>copy2</source>
        <translation type="unfinished">копия2</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="451"/>
        <source>Copy alternative shortcut</source>
        <translation>Копировать альтернативную ссылку</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="454"/>
        <source>Ctrl+Ins</source>
        <translation>Ctrl+Ins</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="470"/>
        <source>About crqt-ng...</source>
        <translation>О crqt-ng...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="491"/>
        <source>Ctrl+B</source>
        <translation>Ctrl+B</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="506"/>
        <source>F6</source>
        <translation>F6</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="521"/>
        <source>Ctrl+F1</source>
        <translation>Ctrl+F1</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="533"/>
        <source>Ctrl+R</source>
        <translation>Ctrl+R</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="548"/>
        <source>Ctrl+F</source>
        <translation>Ctrl+F</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="561"/>
        <source>Export</source>
        <translation>Экспорт</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="564"/>
        <source>Export document</source>
        <translation>Экспортировать документ</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="567"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="572"/>
        <location filename="../mainwindow.ui" line="575"/>
        <location filename="../mainwindow.ui" line="594"/>
        <location filename="../mainwindow.ui" line="597"/>
        <source>Next Page</source>
        <translation>Следующая страница</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="578"/>
        <source>Right</source>
        <translation>Right</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="583"/>
        <location filename="../mainwindow.ui" line="586"/>
        <source>Previous Page</source>
        <translation>Предыдущая страница</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="589"/>
        <source>Left</source>
        <translation>Left</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="600"/>
        <source>Space</source>
        <translation>Space</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="553"/>
        <source>Toggle Edit Mode</source>
        <translation>Переключить режим редактирования</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="556"/>
        <source>Toggle edit mode</source>
        <translation>Переключить режим редактирования</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="605"/>
        <source>Next Sentence</source>
        <translation>Следующее предложение</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="608"/>
        <source>Move selection to next sentence</source>
        <translation>Установить выделение на следующее предложение</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="611"/>
        <source>Ctrl+Right</source>
        <translation>Ctrl+Right</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="622"/>
        <source>Ctrl+Left</source>
        <translation>Ctrl+Left</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="616"/>
        <source>Prev Sentence</source>
        <translation>Предыдущее предложение</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="619"/>
        <source>Select previous sentence</source>
        <translation>Установить выделение на предыдущее предложение</translation>
    </message>
</context>
<context>
    <name>RecentBooksDlg</name>
    <message>
        <location filename="../recentdlg.cpp" line="44"/>
        <source>#</source>
        <translation>№п/п</translation>
    </message>
    <message>
        <location filename="../recentdlg.cpp" line="44"/>
        <source>Author</source>
        <translation>Автор</translation>
    </message>
    <message>
        <location filename="../recentdlg.cpp" line="44"/>
        <source>Title</source>
        <translation>Название</translation>
    </message>
    <message>
        <location filename="../recentdlg.cpp" line="44"/>
        <source>Filename</source>
        <translation>Имя файла</translation>
    </message>
    <message>
        <location filename="../recentdlg.cpp" line="176"/>
        <source>Remove all history items</source>
        <translation>Очистка списка последних книг</translation>
    </message>
    <message>
        <location filename="../recentdlg.cpp" line="177"/>
        <source>Do you really want to remove all history records?</source>
        <translation>Вы действительно ходите удалить все записи из списка последних открытых книг?</translation>
    </message>
    <message>
        <location filename="../recentdlg.ui" line="14"/>
        <source>Recent Books</source>
        <translation>Последние книги</translation>
    </message>
    <message>
        <location filename="../recentdlg.ui" line="41"/>
        <source>Remove Item</source>
        <translation>Удалить запись</translation>
    </message>
    <message>
        <location filename="../recentdlg.ui" line="49"/>
        <source>Clear All</source>
        <translation>Очистить весь список</translation>
    </message>
    <message>
        <location filename="../recentdlg.ui" line="44"/>
        <source>Del</source>
        <translation>Del</translation>
    </message>
</context>
<context>
    <name>SampleView</name>
    <message>
        <location filename="../sampleview.cpp" line="30"/>
        <source>Style Preview</source>
        <translation>Предпросмотр стиля</translation>
    </message>
</context>
<context>
    <name>SearchDialog</name>
    <message>
        <location filename="../searchdlg.cpp" line="144"/>
        <source>Not found</source>
        <translation>Не найдено</translation>
    </message>
    <message>
        <location filename="../searchdlg.cpp" line="144"/>
        <source>Search pattern is not found in document</source>
        <translation>Искомый шаблон не найден в документе</translation>
    </message>
    <message>
        <location filename="../searchdlg.ui" line="14"/>
        <source>Search</source>
        <translation>Поиск</translation>
    </message>
    <message>
        <location filename="../searchdlg.ui" line="25"/>
        <source>Text</source>
        <translation>Текст</translation>
    </message>
    <message>
        <location filename="../searchdlg.ui" line="32"/>
        <source>Case Sensitive</source>
        <translation>Чувствительность к регистру</translation>
    </message>
    <message>
        <location filename="../searchdlg.ui" line="39"/>
        <source>Search forward</source>
        <translation>Искать вперед</translation>
    </message>
    <message>
        <location filename="../searchdlg.ui" line="46"/>
        <source>Search backward</source>
        <translation>Искать назад</translation>
    </message>
    <message>
        <location filename="../searchdlg.ui" line="73"/>
        <source>Find Next</source>
        <translation>Найти следующий</translation>
    </message>
    <message>
        <location filename="../searchdlg.ui" line="83"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
</context>
<context>
    <name>SettingsDlg</name>
    <message>
        <location filename="../settings.cpp" line="429"/>
        <source>[No hyphenation]</source>
        <translation>[Без переносов]</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="431"/>
        <source>[Algorythmic hyphenation]</source>
        <translation>[Алгоритмические переносы]</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1033"/>
        <source>The quick brown fox jumps over the lazy dog. </source>
        <translation>Съешь ещё этих мягких французских булочек! </translation>
    </message>
    <message>
        <location filename="../settings.ui" line="981"/>
        <location filename="../settings.cpp" line="1091"/>
        <source>Text color</source>
        <translation>Цвет текста</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1293"/>
        <location filename="../settings.cpp" line="1095"/>
        <source>Background color</source>
        <translation>Цвет фона</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="497"/>
        <location filename="../settings.cpp" line="1099"/>
        <source>Page header text color</source>
        <translation>Цвет колонтитула</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="23"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="37"/>
        <source>Window options</source>
        <translation>Настройки окна</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="43"/>
        <source>Window</source>
        <translation>Окно</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="51"/>
        <source>Look &amp; feel</source>
        <translation>Стиль интерфейса</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="61"/>
        <source>Startup action</source>
        <translation>Действие при старте</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="74"/>
        <source>Show list of recent books</source>
        <translation>Показать список последних книг</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="79"/>
        <source>Show File Open dialog</source>
        <translation>Показать диалог открытия файла</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="84"/>
        <source>Do nothing</source>
        <translation>Ничего не делать</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="92"/>
        <source>Controls</source>
        <translation>Управление</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="101"/>
        <source>Show toolbar</source>
        <translation>Показать панель инструментов</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="108"/>
        <source>Show menu</source>
        <translation>Показать меню</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="115"/>
        <source>Show scroll bar</source>
        <translation>Показать полосу прокрутки</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="122"/>
        <source>Show status bar</source>
        <translation>Показать строку состояния</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="138"/>
        <source>Automatically when selected:</source>
        <translation>Автоматически при выделении:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="148"/>
        <source>Copy to clipboard</source>
        <translation>Скопировать в буфер обмена</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="155"/>
        <source>Run command:</source>
        <translation>Выполнить команду:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="169"/>
        <source>goldendict %TEXT%, qolibri %TEXT%, etc.</source>
        <translation>goldendict %TEXT%, qolibri %TEXT%, и т.д.</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="178"/>
        <source>Fullscreen display</source>
        <translation>Полно-экранный режим</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="202"/>
        <source>Page</source>
        <translation>Страница</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="220"/>
        <source>One page</source>
        <translation>Одна страница</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="225"/>
        <source>Two pages</source>
        <translation>Две страницы</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="230"/>
        <source>Scroll View</source>
        <translation>Свиток</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="244"/>
        <source>Vew Mode</source>
        <translation>Режим просмотра</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="257"/>
        <source>Page margins</source>
        <translation>Отступы страницы</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="378"/>
        <source>Show page header</source>
        <translation>Показывать заголовок страницы</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="412"/>
        <source>Book name</source>
        <translation>Название книги</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="440"/>
        <source>Clock</source>
        <translation>Часы</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="447"/>
        <source>Battery status</source>
        <translation>Состояние батареи</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="433"/>
        <source>Position percent</source>
        <translation>Процент прочитанного</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="480"/>
        <source>Header font</source>
        <translation>Шрифт заголовка</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="512"/>
        <location filename="../settings.ui" line="578"/>
        <location filename="../settings.ui" line="655"/>
        <location filename="../settings.ui" line="706"/>
        <location filename="../settings.ui" line="996"/>
        <location filename="../settings.ui" line="1308"/>
        <source>Change</source>
        <translation>Изменить</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="556"/>
        <source>Show footnotes at bottom of page</source>
        <translation>Показывать сноски внизу страницы</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="970"/>
        <source>Styles</source>
        <translation>Стили</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1420"/>
        <location filename="../settings.ui" line="1434"/>
        <source>Hyphenation</source>
        <translation>Переносы</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1458"/>
        <source>Interline spacing</source>
        <translation>Междустрочный интервал</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1258"/>
        <source>Page skin</source>
        <translation>Стиль страницы</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1548"/>
        <source>.TXT files</source>
        <translation>.TXT файлы</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1555"/>
        <source>Disable automatic formatting</source>
        <translation>Отключить авто форматирование</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="265"/>
        <location filename="../settings.ui" line="781"/>
        <location filename="../settings.ui" line="831"/>
        <location filename="../settings.ui" line="881"/>
        <location filename="../settings.ui" line="931"/>
        <location filename="../settings.cpp" line="591"/>
        <location filename="../settings.cpp" line="632"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="290"/>
        <source>5</source>
        <translation>5</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="305"/>
        <source>8</source>
        <translation>8</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="315"/>
        <source>10</source>
        <translation>10</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="330"/>
        <source>15</source>
        <translation>15</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="335"/>
        <source>20</source>
        <translation>20</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="340"/>
        <source>25</source>
        <translation>25</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="345"/>
        <source>30</source>
        <translation>30</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1468"/>
        <source>Min space width</source>
        <translation>Мин. ширина пробела</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1569"/>
        <source>Font families</source>
        <translation>Семейства шрифтов</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1588"/>
        <location filename="../settings.cpp" line="456"/>
        <source>Default paragraph style</source>
        <translation>Стиль параграфа</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="457"/>
        <source>Title</source>
        <translation>Заголовок</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="458"/>
        <source>Subtitle</source>
        <translation>Подзаголовок</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="459"/>
        <source>Preformatted text</source>
        <translation>Пред-форматированный текст</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="460"/>
        <source>Link</source>
        <translation>Ссылка</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="461"/>
        <source>Cite / quotation</source>
        <translation>Цитата</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="462"/>
        <source>Epigraph</source>
        <translation>Эпиграф</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="463"/>
        <source>Poem</source>
        <translation>Поэма</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="464"/>
        <source>Text author</source>
        <translation>Автор текста</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="465"/>
        <source>Footnote link</source>
        <translation>Ссылка на сноску</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="466"/>
        <source>Footnote</source>
        <translation>Сноска</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="467"/>
        <source>Footnote title</source>
        <translation>Заголовок сноски</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="468"/>
        <source>Annotation</source>
        <translation>Аннотация</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="545"/>
        <location filename="../settings.cpp" line="561"/>
        <location filename="../settings.cpp" line="590"/>
        <location filename="../settings.cpp" line="631"/>
        <location filename="../settings.cpp" line="664"/>
        <location filename="../settings.cpp" line="686"/>
        <location filename="../settings.cpp" line="705"/>
        <location filename="../settings.cpp" line="751"/>
        <location filename="../settings.cpp" line="816"/>
        <location filename="../settings.cpp" line="834"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="545"/>
        <source>Justify</source>
        <translation>Выравнивание</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1749"/>
        <location filename="../settings.cpp" line="545"/>
        <source>Left</source>
        <translation>Слева</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="545"/>
        <source>Center</source>
        <translation>По центру</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1759"/>
        <location filename="../settings.cpp" line="545"/>
        <source>Right</source>
        <translation>Справа</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="561"/>
        <source>No indent</source>
        <translation>Без отступа</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="561"/>
        <source>Small Indent</source>
        <translation>Маленький отступ</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="561"/>
        <source>Big Indent</source>
        <translation>Большой отступ</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="561"/>
        <source>Small Outdent</source>
        <translation>Маленький выступ</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="561"/>
        <source>Big Outdent</source>
        <translation>Большой выступ</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="592"/>
        <source>20% of line height</source>
        <translation>20% от высоты строки</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="593"/>
        <source>30% of line height</source>
        <translation>30% от высоты строки</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="594"/>
        <location filename="../settings.cpp" line="633"/>
        <source>50% of line height</source>
        <translation>50% от высоты строки</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="595"/>
        <location filename="../settings.cpp" line="634"/>
        <source>100% of line height</source>
        <translation>100% от высоты строки</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="596"/>
        <location filename="../settings.cpp" line="635"/>
        <source>150% of line height</source>
        <translation>150% от высоты строки</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="636"/>
        <source>200% of line height</source>
        <translation>200% от высоты строки</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="637"/>
        <source>400% of line height</source>
        <translation>400% от высоты строки</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="638"/>
        <source>5% of line width</source>
        <translation>5% от ширины строки</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="639"/>
        <source>10% of line width</source>
        <translation>10% от ширины строки</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="640"/>
        <source>15% of line width</source>
        <translation>15% от ширины строки</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="641"/>
        <source>20% of line width</source>
        <translation>20% от ширины строки</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="642"/>
        <source>30% of line width</source>
        <translation>30% от ширины строки</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="665"/>
        <location filename="../settings.cpp" line="706"/>
        <source>Normal</source>
        <translation>Обычный</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="666"/>
        <source>Bold</source>
        <translation>Полужирный</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="667"/>
        <source>Bolder</source>
        <translation>Жирный</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="668"/>
        <source>Lighter</source>
        <translation>Тонкий</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="687"/>
        <source>Increase: 110%</source>
        <translation>Увеличить: 110%</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="688"/>
        <source>Increase: 120%</source>
        <translation>Увеличить: 120%</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="689"/>
        <source>Increase: 150%</source>
        <translation>Увеличить: 150%</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="690"/>
        <source>Decrease: 90%</source>
        <translation>Уменьшить: 90%</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="691"/>
        <source>Decrease: 80%</source>
        <translation>Уменьшить: 80%</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="692"/>
        <source>Decrease: 70%</source>
        <translation>Уменьшить: 70%</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="693"/>
        <source>Decrease: 60%</source>
        <translation>Уменьшить: 60%</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="707"/>
        <source>Italic</source>
        <translation>Курсив</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="716"/>
        <source>[Default Sans Serif]</source>
        <translation>[рубленый шрифт без засечек]</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="718"/>
        <source>[Default Serif]</source>
        <translation>[рубленый шрифт с засечками]</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="720"/>
        <source>[Default Monospace]</source>
        <translation>[моноширинный шрифт]</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="752"/>
        <source>Black</source>
        <translation>Чёрный</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="753"/>
        <source>Green</source>
        <translation>Зелёный</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="754"/>
        <source>Silver</source>
        <translation>Серебристый</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="755"/>
        <source>Lime</source>
        <translation>Лимоновый</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="245"/>
        <location filename="../settings.cpp" line="252"/>
        <location filename="../settings.cpp" line="756"/>
        <source>Gray</source>
        <translation>Серый</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="757"/>
        <source>Olive</source>
        <translation>Оливковый</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="758"/>
        <source>White</source>
        <translation>Белый</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="759"/>
        <source>Yellow</source>
        <translation>Жёлтый</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="760"/>
        <source>Maroon</source>
        <translation>Каштановый</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="761"/>
        <source>Navy</source>
        <translation>Темно-синий</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="762"/>
        <source>Red</source>
        <translation>Красный</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="763"/>
        <source>Blue</source>
        <translation>Синий</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="764"/>
        <source>Purple</source>
        <translation>Пурпурный</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="765"/>
        <source>Teal</source>
        <translation>Сине-зелёный</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="766"/>
        <source>Fuchsia</source>
        <translation>Фуксия</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="767"/>
        <source>Aqua</source>
        <translation>Зеленовато-голубой</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="386"/>
        <location filename="../settings.cpp" line="244"/>
        <location filename="../settings.cpp" line="251"/>
        <location filename="../settings.cpp" line="817"/>
        <source>None</source>
        <translation>Нет</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="634"/>
        <location filename="../settings.cpp" line="818"/>
        <source>Underline</source>
        <translation>Подчеркнутый</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="819"/>
        <source>Line through</source>
        <translation>Перечеркнутый</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="820"/>
        <source>Overline</source>
        <translation>Надчеркнутый</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="835"/>
        <source>Baseline</source>
        <translation>По снованию</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="836"/>
        <source>Subscript</source>
        <translation>Нижний индекс</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="837"/>
        <source>Superscript</source>
        <translation>Верхний индекс</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1492"/>
        <source>Floating punctuation</source>
        <translation>Висячая пунктуация</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1499"/>
        <source>Enable floating punctuation</source>
        <translation>Разрешить висячую пунктуацию</translation>
    </message>
    <message>
        <source>0.3</source>
        <translation type="vanished">0.3</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="69"/>
        <source>Restore session</source>
        <translation>Восстановить сессию</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="129"/>
        <source>Fixed tab size</source>
        <translation>Фиксированная ширина вкладок</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="145"/>
        <source>automatically copy text to clipboard when selected</source>
        <translation>При выделении текста автоматически копировать в буфер обмена</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="162"/>
        <source>Automatically send selected text to this application.
%TEXT% will be replaced with the selected text.</source>
        <translation>Автоматически передать выделенный текст в это приложение.
%TEXT% будет заменён выделенным текстом.</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="406"/>
        <source>Header elements</source>
        <translation>Элементы колонтитула</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="419"/>
        <source>Page number</source>
        <translation>Номер страницы</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="426"/>
        <source>Page count</source>
        <translation>Количество страниц</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1054"/>
        <source>Default font</source>
        <translation>Шрифт по умолчанию</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1136"/>
        <source>Font gamma</source>
        <translation>Гамма шрифта</translation>
    </message>
    <message>
        <source>0.35</source>
        <translation type="vanished">0.35</translation>
    </message>
    <message>
        <source>0.4</source>
        <translation type="vanished">0.4</translation>
    </message>
    <message>
        <source>0.45</source>
        <translation type="vanished">0.45</translation>
    </message>
    <message>
        <source>0.5</source>
        <translation type="vanished">0.5</translation>
    </message>
    <message>
        <source>0.55</source>
        <translation type="vanished">0.55</translation>
    </message>
    <message>
        <source>0.6</source>
        <translation type="vanished">0.6</translation>
    </message>
    <message>
        <source>0.65</source>
        <translation type="vanished">0.65</translation>
    </message>
    <message>
        <source>0.7</source>
        <translation type="vanished">0.7</translation>
    </message>
    <message>
        <source>0.75</source>
        <translation type="vanished">0.75</translation>
    </message>
    <message>
        <source>0.8</source>
        <translation type="vanished">0.8</translation>
    </message>
    <message>
        <source>0.85</source>
        <translation type="vanished">0.85</translation>
    </message>
    <message>
        <source>0.9</source>
        <translation type="vanished">0.9</translation>
    </message>
    <message>
        <source>0.95</source>
        <translation type="vanished">0.95</translation>
    </message>
    <message>
        <source>0.98</source>
        <translation type="vanished">0.98</translation>
    </message>
    <message>
        <source>1.0</source>
        <translation type="vanished">1.0</translation>
    </message>
    <message>
        <source>1.02</source>
        <translation type="vanished">1.02</translation>
    </message>
    <message>
        <source>1.05</source>
        <translation type="vanished">1.05</translation>
    </message>
    <message>
        <source>1.1</source>
        <translation type="vanished">1.1</translation>
    </message>
    <message>
        <source>1.15</source>
        <translation type="vanished">1.15</translation>
    </message>
    <message>
        <source>1.2</source>
        <translation type="vanished">1.2</translation>
    </message>
    <message>
        <source>1.25</source>
        <translation type="vanished">1.25</translation>
    </message>
    <message>
        <source>1.3</source>
        <translation type="vanished">1.3</translation>
    </message>
    <message>
        <source>1.35</source>
        <translation type="vanished">1.35</translation>
    </message>
    <message>
        <source>1.4</source>
        <translation type="vanished">1.4</translation>
    </message>
    <message>
        <source>1.45</source>
        <translation type="vanished">1.45</translation>
    </message>
    <message>
        <source>1.5</source>
        <translation type="vanished">1.5</translation>
    </message>
    <message>
        <source>1.6</source>
        <translation type="vanished">1.6</translation>
    </message>
    <message>
        <source>1.7</source>
        <translation type="vanished">1.7</translation>
    </message>
    <message>
        <source>1.8</source>
        <translation type="vanished">1.8</translation>
    </message>
    <message>
        <source>1.9</source>
        <translation type="vanished">1.9</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1188"/>
        <source>Font hinting</source>
        <translation>Хинтинг</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1220"/>
        <source>No hinting</source>
        <translation>Без хинтинга</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1225"/>
        <source>Use bytecode</source>
        <translation>Использовать байт-код</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1230"/>
        <source>Autohinting</source>
        <translation>Автохинтинг</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1238"/>
        <source>Fallback fonts</source>
        <translation>Резервные шрифты</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1513"/>
        <source>Enable document internal styles</source>
        <translation>Разрешить встроенные стили</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1506"/>
        <source>Internal CSS</source>
        <translation>Внутренние таблицы стилей</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1527"/>
        <source>Enable document embedded fonts</source>
        <translation>Разрешить встроенные шрифты</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1520"/>
        <source>.EPUB</source>
        <translation>Для файлов .EPUB</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1622"/>
        <source>Alignment</source>
        <translation>Выравнивание</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1632"/>
        <source>First line</source>
        <translation>Первая строка</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1642"/>
        <source>Font size</source>
        <translation>Размер шрифта</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1652"/>
        <source>Font face</source>
        <translation>Начертание шрифта</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1119"/>
        <location filename="../settings.ui" line="1662"/>
        <source>Font weight</source>
        <translation>Насыщенность шрифта</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1672"/>
        <source>Font style</source>
        <translation>Стиль шрифта</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1682"/>
        <source>Font color</source>
        <translation>Цвет шрифта</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1722"/>
        <source>Margins:</source>
        <translation>Отступы:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1729"/>
        <source>Before</source>
        <translation>Перед</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1739"/>
        <source>After</source>
        <translation>После</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1695"/>
        <source>Interline space</source>
        <translation>Междустрочный интервал</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1705"/>
        <source>Text decoration</source>
        <translation>Оформление текста</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1715"/>
        <source>Vertical align</source>
        <translation>Вертикальное выравнивание</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1579"/>
        <source>Stylesheet</source>
        <translation>Таблицы стилей</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="565"/>
        <location filename="../settings.cpp" line="1353"/>
        <source>Selection color</source>
        <translation>Цвет выделения</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1357"/>
        <source>Comment bookmark color</source>
        <translation>Цвет комментариев</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1361"/>
        <source>Correction bookmark color</source>
        <translation>Цвет исправлений</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="642"/>
        <source>Comment color</source>
        <translation>Цвет комментариев</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="693"/>
        <source>Correction color</source>
        <translation>Цвет исправлений</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="616"/>
        <source>Bookmark highlight</source>
        <translation>Подсветка закладок</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="624"/>
        <location filename="../settings.ui" line="762"/>
        <location filename="../settings.ui" line="812"/>
        <location filename="../settings.ui" line="862"/>
        <location filename="../settings.ui" line="912"/>
        <source>Disabled</source>
        <translation>Отключить</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="629"/>
        <source>Solid fill</source>
        <translation>Цвет фона</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="754"/>
        <source>Inline images zoom in</source>
        <translation>Инлайн картинки, увеличение</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="767"/>
        <location filename="../settings.ui" line="817"/>
        <location filename="../settings.ui" line="867"/>
        <location filename="../settings.ui" line="917"/>
        <source>Integer scale</source>
        <translation>В целое число раз</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="772"/>
        <location filename="../settings.ui" line="822"/>
        <location filename="../settings.ui" line="872"/>
        <location filename="../settings.ui" line="922"/>
        <source>Arbitrary scale</source>
        <translation>Произвольный масштаб</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="270"/>
        <location filename="../settings.ui" line="786"/>
        <location filename="../settings.ui" line="836"/>
        <location filename="../settings.ui" line="886"/>
        <location filename="../settings.ui" line="936"/>
        <source>1</source>
        <translation>х1</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="275"/>
        <location filename="../settings.ui" line="791"/>
        <location filename="../settings.ui" line="841"/>
        <location filename="../settings.ui" line="891"/>
        <location filename="../settings.ui" line="941"/>
        <source>2</source>
        <translation>х2</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="280"/>
        <location filename="../settings.ui" line="796"/>
        <location filename="../settings.ui" line="846"/>
        <location filename="../settings.ui" line="896"/>
        <location filename="../settings.ui" line="946"/>
        <source>3</source>
        <translation>х3</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="854"/>
        <source>Block images zoom in</source>
        <translation>Обычные картинки, увеличеие</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="904"/>
        <source>Block images zoom out</source>
        <translation>Обычные картинки, уменьшение</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="804"/>
        <source>Inline images zoom out</source>
        <translation>Инлайн картинки, уменьшение</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="744"/>
        <source>Image scaling options</source>
        <translation>Масштабирование картинок</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="285"/>
        <source>4</source>
        <translation>4</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="295"/>
        <source>6</source>
        <translation>6</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="300"/>
        <source>7</source>
        <translation>7</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="310"/>
        <source>9</source>
        <translation>9</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="320"/>
        <source>11</source>
        <translation>11</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="325"/>
        <source>12</source>
        <translation>12</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="350"/>
        <source>40</source>
        <translation>40</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="355"/>
        <source>50</source>
        <translation>50</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="360"/>
        <source>60</source>
        <translation>60</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1174"/>
        <source>Font antialiasing</source>
        <translation>Сглаживание шрифта</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1444"/>
        <source>Text shaping</source>
        <translation>Формировщик текста</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1478"/>
        <source>Font kerning</source>
        <translation>Кернинг шрифта</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1485"/>
        <source>Enable font kerning</source>
        <translation>Разрешить кернинг шрифта</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1354"/>
        <source>Rendering flags</source>
        <translation>Флаги рендеринга</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1362"/>
        <location filename="../settings.ui" line="1393"/>
        <source>Legacy</source>
        <translation>Legacy</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1367"/>
        <source>Flat</source>
        <translation>Flat</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1372"/>
        <source>Book</source>
        <translation>Book</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1377"/>
        <source>Web (Full)</source>
        <translation>Web (Full)</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1385"/>
        <source>DOM level:</source>
        <translation>уровент поддержки DOM:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1398"/>
        <source>Newest</source>
        <translation>Новейший</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1406"/>
        <source>Multi languages</source>
        <translation>многоязычность</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1413"/>
        <source>Support for multilingual documents</source>
        <translation>Поддержка многоязычных документов</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1251"/>
        <location filename="../settings.ui" line="1562"/>
        <source>Manage...</source>
        <translation>Настроить...</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="216"/>
        <source>Simple (FreeType only, fastest)</source>
        <translation>Простой (FreeType only, быстрый)</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="217"/>
        <source>Light (HarfBuzz without ligatures)</source>
        <translation>Легкий (HarfBuzz, без лигатур)</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="218"/>
        <source>Full (HarfBuzz with ligatures)</source>
        <translation>Полный (HarfBuzz, с лигатурами)</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1427"/>
        <source>Enable hyphenation</source>
        <translation>Разрешить переносы</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="391"/>
        <source>Page header</source>
        <translation>верхний колонтитул страницы</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="396"/>
        <source>Page footer</source>
        <translation>нижний колонтитул страницы</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1541"/>
        <source>Ignore document margins</source>
        <translation>Игнорировать отступы документа</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="246"/>
        <source>LCD (RGB)</source>
        <translation>LCD (RGB)</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="247"/>
        <source>LCD (BGR)</source>
        <translation>LCD (BGR)</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="248"/>
        <source>LCD (RGB) vertical</source>
        <translation>LCD (RGB) вертикально</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="249"/>
        <source>LCD (BGR) vertical</source>
        <translation>LCD (BGR) вертикально</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1202"/>
        <source>synthetic*</source>
        <translation>синтетически*</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1204"/>
        <source>synthetic</source>
        <translation>синтетически</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1534"/>
        <source>Margins redefine</source>
        <translation>Переопределение отступов</translation>
    </message>
</context>
<context>
    <name>TocDlg</name>
    <message>
        <location filename="../tocdlg.cpp" line="75"/>
        <source>Title</source>
        <translation>Название</translation>
    </message>
    <message>
        <location filename="../tocdlg.cpp" line="75"/>
        <source>Page</source>
        <translation>Страница</translation>
    </message>
    <message>
        <location filename="../tocdlg.ui" line="14"/>
        <source>Table of Contents</source>
        <translation>Содержание</translation>
    </message>
</context>
<context>
    <name>WolExportDlg</name>
    <message>
        <location filename="../wolexportdlg.ui" line="14"/>
        <source>Dialog</source>
        <translation>Диалог</translation>
    </message>
    <message>
        <location filename="../wolexportdlg.ui" line="22"/>
        <source>Bits per pixel</source>
        <translation>Бит на пиксель</translation>
    </message>
    <message>
        <location filename="../wolexportdlg.ui" line="30"/>
        <location filename="../wolexportdlg.ui" line="51"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="../wolexportdlg.ui" line="35"/>
        <location filename="../wolexportdlg.ui" line="56"/>
        <source>2</source>
        <translation>2</translation>
    </message>
    <message>
        <location filename="../wolexportdlg.ui" line="43"/>
        <source>Table of Contents levels</source>
        <translation>Уровней заголовка</translation>
    </message>
    <message>
        <location filename="../wolexportdlg.ui" line="61"/>
        <source>3</source>
        <translation>3</translation>
    </message>
</context>
<context>
    <name>crqtutils</name>
    <message>
        <location filename="../crqtutil.cpp" line="184"/>
        <source>Undetermined</source>
        <translation>Неопределенный</translation>
    </message>
</context>
</TS>
