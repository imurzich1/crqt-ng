cmake_minimum_required(VERSION 3.5 FATAL_ERROR)

project(crqt-ng)

set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake/modules/")

set(APPLICATION_NAME "crqt-ng")

#  VERSION_DEV_A/CODE description:
# alphaX - 0X; beta X - 1X; rcX - 2X; releaseX - 3X (or empty)
set(VERSION_MAJOR 1)
set(VERSION_MINOR 0)
set(VERSION_PATCH 15)
set(VERSION_DEV_A "")
set(VERSION_DEV_CODE 30)
set(VERSION "${VERSION_MAJOR}.${VERSION_MINOR}.${VERSION_PATCH}")
if (VERSION_DEV_A)
  set(VERSION "${VERSION}-${VERSION_DEV_A}")
endif()
set(PACKAGE_VERSION ${VERSION})
# for windows rc file
set(PRODUCTVERSION_V "${VERSION_MAJOR},${VERSION_MINOR},${VERSION_PATCH},${VERSION_DEV_CODE}")
set(FILEVERSION_V ${PRODUCTVERSION_V})

set(PACKAGE_DATADIR	${CMAKE_INSTALL_PREFIX}/share)

option(ADD_DEBUG_EXTRA_OPTS "Add extra debug flags and technique" OFF)
set(USE_QT QT5 CACHE STRING "Qt version to use")
set_property(CACHE USE_QT PROPERTY STRINGS QT5 QT6)
option(STATIC_CRENGINE_NG "Link with the static version of crengine-ng" OFF)

if (APPLE)
  if( ${CMAKE_SYSTEM} MATCHES "Darwin" )
    set(MACOS 1)
  endif()
endif()

configure_file(config.h.cmake ${CMAKE_BINARY_DIR}/config.h)

if( ${USE_QT} STREQUAL QT6 )
  # Find the Qt6 libraries
  find_package(Qt6Core REQUIRED)
  find_package(Qt6Gui REQUIRED)
  find_package(Qt6Widgets REQUIRED)
  include(Qt6TranslationToolsMacros)
elseif( ${USE_QT} STREQUAL QT5 )
  # Find the Qt5 libraries
  find_package(Qt5Core REQUIRED)
  find_package(Qt5Gui REQUIRED)
  find_package(Qt5Widgets REQUIRED)
  include(Qt5TranslationToolsMacros)
endif()

# crengine-ng
find_package(crengine-ng 0.9.12 REQUIRED)

include_directories(${CRENGINE_NG_INCLUDE_DIR})

# Add include path for config.h
include_directories(${CMAKE_BINARY_DIR})

# force use exceptions
set(CMAKE_CXX_FLAGS         "${CMAKE_CXX_FLAGS} -fexceptions")
# add debug defines
set(CMAKE_C_FLAGS_DEBUG     "${CMAKE_C_FLAGS_DEBUG}     -D_DEBUG")
set(CMAKE_CXX_FLAGS_DEBUG   "${CMAKE_CXX_FLAGS_DEBUG}   -D_DEBUG")
set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -DNDEBUG")

if(ADD_DEBUG_EXTRA_OPTS)
	add_definitions(-D_GLIBCXX_DEBUG -D_GLIBCXX_DEBUG_PEDANTIC)
	set(DEBUG_EXTRA_FLAGS "-fstack-check -fstack-protector -fno-omit-frame-pointer -Wshift-overflow=2")
	if ("${CMAKE_SYSTEM_NAME}" STREQUAL "Linux")
		set (DEBUG_EXTRA_FLAGS "${DEBUG_EXTRA_FLAGS} -fsanitize=address -fsanitize=undefined -fno-sanitize-recover -fno-sanitize=alignment")
		set(CMAKE_EXE_LINKER_FLAGS_DEBUG "${CMAKE_EXE_LINKER_FLAGS_DEBUG} -fsanitize=address -fsanitize=undefined -fno-sanitize-recover -fstack-check -fstack-protector")
		set(CMAKE_SHARED_LINKER_FLAGS_DEBUG "${CMAKE_EXE_LINKER_FLAGS_DEBUG} -fsanitize=address -fsanitize=undefined -fno-sanitize-recover -fstack-check -fstack-protector")
	endif ("${CMAKE_SYSTEM_NAME}" STREQUAL "Linux")
	set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${DEBUG_EXTRA_FLAGS}")
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${DEBUG_EXTRA_FLAGS}")
endif(ADD_DEBUG_EXTRA_OPTS)

if(UNIX)
  add_definitions(-DCRUI_DATA_DIR="${CMAKE_INSTALL_PREFIX}/share/crqt/")
else()
  add_definitions(-DCRUI_DATA_DIR="")
endif()

if (WIN32)
	set(CMAKE_RC_COMPILER windres)
	# set rc syntax
	set(CMAKE_RC_COMPILE_OBJECT "<CMAKE_RC_COMPILER> <FLAGS> <DEFINES> -O coff -o <OBJECT> -i <SOURCE>")
	set(CMAKE_RC_SOURCE_FILE_EXTENSIONS rc)
	set(CMAKE_RC_FLAGS "-I${CMAKE_BINARY_DIR}")
	# enable resource language for this project
	enable_language(RC)
endif(WIN32)

add_subdirectory(src)


# Packaging
set(CPACK_PACKAGE_NAME "${APPLICATION_NAME}")
set(CPACK_PACKAGE_VERSION_MAJOR "${VERSION_MAJOR}")
set(CPACK_PACKAGE_VERSION_MINOR "${VERSION_MINOR}")
set(CPACK_PACKAGE_VERSION_PATCH "${VERSION_PATCH}")
set(CPACK_PACKAGE_VENDOR "Aleksey Chernov  <valexlin@gmail.com>")
set(CPACK_PACKAGE_CONTACT "${CPACK_PACKAGE_VENDOR}")
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "crqt-ng - cross-platform open source e-book reader using crengine-ng.")
set(CPACK_RESOURCE_FILE_LICENSE "${CMAKE_SOURCE_DIR}/LICENSE")
set(CPACK_PACKAGE_EXECUTABLES "crqt;${CMAKE_PROJECT_NAME}")
set(CPACK_CREATE_DESKTOP_LINKS "crqt-ng")
set(CPACK_DEBIAN_PACKAGE_SECTION "graphics")
if (WIN32)
  set(CPACK_NSIS_INSTALLED_ICON_NAME "crqt.exe")
  set(CPACK_NSIS_PACKAGE_NAME "${CMAKE_PROJECT_NAME}")
  set(CPACK_NSIS_DISPLAY_NAME "${CMAKE_PROJECT_NAME} ${VERSION}")
  set(CPACK_NSIS_ENABLE_UNINSTALL_BEFORE_INSTALL ON)
  set(CPACK_NSIS_EXECUTABLES_DIRECTORY "${CMAKE_BINARY_DIR}")
  set(CPACK_PACKAGE_INSTALL_DIRECTORY "${CMAKE_PROJECT_NAME}")
  set(CPACK_NSIS_CREATE_ICONS_EXTRA
      "CreateShortCut \\\"$DESKTOP\\\\${CMAKE_PROJECT_NAME}.lnk\\\" \\\"$INSTDIR\\\\crqt.exe\\\"")
  set(CPACK_NSIS_DELETE_ICONS_EXTRA
      "Delete \\\"$DESKTOP\\\\${CMAKE_PROJECT_NAME}.lnk\\\"")
endif()
set(CPACK_SOURCE_PACKAGE_FILE_NAME "${APPLICATION_NAME}-${VERSION}")
set(CPACK_SOURCE_IGNORE_FILES
    "/\\\\.svn/"
    "/\\\\.git/"
    "~$"
    "\\\\.pcs$"
    "TODO.txt"
    "CMakeLists.txt.user"
    "/doxygen/"
    "${CMAKE_BINARY_DIR}")
include(CPack)
